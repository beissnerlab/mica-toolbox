#!/usr/bin/env python

# Copyright (C) 2015 Tawfik Moher Alsady, Jorge Manuel and Florian Beissner
#
# beissner.florian@mh-hannover.de
#
# Developed at
# Somatosensory and Autonomic Therapy Research,
# Institute for Neuroradiology, Hannover Medical School,
# Hannover, Germany
#
# This file is part of the mICA toolbox.
#
# The mICA toolbox is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The mICA toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with the mICA toolbox.  If not, see <http://www.gnu.org/licenses/>. 


# This file performs the second step of reproducibility analysis
# cross-correlation matrix -> hungarian sorting -> mean reproducibility -> export results as text table and graphical plot

import sys, math, os, re, subprocess

import matplotlib.pyplot as plt
import numpy as np

import scipy.stats as ss

# import Munkres library for Hungarian sorting
from munkres import Munkres, make_cost_matrix


# function to detect if file empty or not
def is_non_zero_file(fpath):
	return True if os.path.isfile(fpath) and os.path.getsize(fpath) > 0 else False

# parse command-line args
if len(sys.argv) < 4:
	sys.exit("""Usage: ic_corr.py <in_prefix> <samples> <dims> [just_read]

in_prefix		path of the folder that contains samples folders
samples			number of samplings
dims			dimensionality range
[just_read]		0 is default. 1 will not perform fsl_cc and just reads any previously calculated cross-correlation file""")

main_folder = str(sys.argv[1])

trialscount = int(sys.argv[2])
if trialscount == 0: trialscount=1

if len(sys.argv) == 5:
	justread = int(sys.argv[4])
else:
	justread = 0

# convert the given dimensionality argument into list of values
dimsstr = str(sys.argv[3])
dimsstr_split = dimsstr.split(",")
dims=[]
for dim in dimsstr_split:
	if dim.find("-") > -1:
		minmax = dim.split("-")
		dim=range(int(minmax[0]),int(minmax[1])+1)
		dims = dims + dim
	else:
		dims.append(int(dim))

# create empty matrix to save reproducibilty results
cor_means_mat = [[0 for x in range(trialscount)] for x in range(len(dims))] 

prefix_fsl = "$FSLDIR/bin/fslcc -t -1 -p 5 -m "

# loop through trials
for trial_num in range(0, trialscount):

	# loop through dimensionality list
	for dim_num, dim in enumerate(dims):

		trial_id = ("%04d" % (trial_num+1))
		g1_mask = main_folder + "/sample_" + trial_id + "/group1/dim" + str(dim) + "/mask.nii.gz"

		ICs_g1 = main_folder + "/sample_" + trial_id + "/group1/dim" + str(dim) + "/melodic_IC.nii.gz"
		ICs_g2 = main_folder + "/sample_" + trial_id + "/group2/dim" + str(dim) + "/melodic_IC.nii.gz"

		out_txt = main_folder + "/sample_" + trial_id + "/corr_dim" + str(dim) + ".txt"

		# check if both melodic_IC files are valid
		# in not, then some error have occured (e.g. non convergence) -> save reproducibilty as NaN
		proc1 = subprocess.Popen("$FSLDIR/bin/imtest "+ ICs_g1, stdout=subprocess.PIPE, shell=True)
		proc2 = subprocess.Popen("$FSLDIR/bin/imtest "+ ICs_g2, stdout=subprocess.PIPE, shell=True)
		n1 = subprocess.Popen("$FSLDIR/bin/fslnvols "+ ICs_g1, stdout=subprocess.PIPE, shell=True)
		n2 = subprocess.Popen("$FSLDIR/bin/fslnvols "+ ICs_g2, stdout=subprocess.PIPE, shell=True)
		if int(proc1.stdout.read().rstrip('\n')) != 1 or int(proc2.stdout.read().rstrip('\n')) != 1 or int(n1.stdout.read().rstrip('\n')) != int(dim) or int(n2.stdout.read().rstrip('\n')) != int(dim):
			cor_means_mat[dim_num][trial_num] = np.nan
			continue

		# perform fslcc (calculate cross-correlation matrix)
		if not justread:
			os.system(prefix_fsl + g1_mask + " " + ICs_g1 + " " + ICs_g2 + " > " + out_txt)

		# check if fslcc resulted in a nonempty output file
		# if yes, read it and convert correlation values to matrix
		# if no, save reproducibilty as NaN (some error occured) -> save reproducibilty as NaN
		if is_non_zero_file(out_txt):
			with open(out_txt) as f:
				table = [re.sub(' +',' ', line).rstrip('\n').strip().split(' ') for line in f.readlines()]

			vol_col = [int(col[0]) for col in table]
			r_col=[float(col[2]) for col in table]

			# cross-correlation matrix
			dim_val = dim if dim > 0 else max(vol_col)
			cor_mat = [r_col[i:i+dim_val] for i in range(0, len(r_col), dim_val)]

			m = Munkres()
			# nullify negative values and subtract each value from 1 to calculate minimum cost
			cost_matrix = make_cost_matrix(cor_mat, lambda cost: ( (1-cost) if cost > 0 else 1) )

			# perform hungarian sorting
			indexes = m.compute(cost_matrix)

			total = 0
			for row, column in indexes:
				value = cor_mat[row][column]
				total += value
				#print '(%d, %d) -> %f' % (row, column, value)
			#print 'total cost: %f' % total

			# save mean diagonal correlation as reproducibility for this trial and dimensionality
			cor_means_mat[dim_num][trial_num] =  total / dim_val

		else:
			cor_means_mat[dim_num][trial_num] = np.nan
			continue

# write reproducibility values for all trials and dimensionalities as text file
with open(main_folder + "/corr.csv", 'w') as f:
	for s in cor_means_mat:
		f.write(",".join( ["{0:0.4f}".format(i) for i in s]) + '\n')

# calculate mean reproducibility (from all trials) for each dimensionality
# and count any errors (e.g. non convergence)
final_means = []
final_sd = []
final_errors = []
for dim_means in cor_means_mat:
	dim_means_np = np.array(dim_means)
	dim_means_np_nonan = dim_means_np[~np.isnan(dim_means_np)]
	final_means.append(dim_means_np_nonan.mean())
	final_sd.append(dim_means_np_nonan.std())
	final_errors.append(sum(np.isnan(dim_means_np))/float(trialscount))

data_m=np.array(final_means)
data_sd=np.array(final_sd)
data_df=np.array([trialscount-1] * len(dims))
z_ci = ss.norm.isf(0.05/2)  # Two-tailed 95%
data_se = data_sd/math.sqrt(trialscount)

# write reproducibility mean and sd as text file
np.savetxt(main_folder + "/corr_mean_sd.csv", np.column_stack((data_m,data_sd)), fmt="%0.4f", delimiter=",", newline="\n")

# plot results and save it as png and svg files
plotwidth = (0.1 * len(dims) )
if plotwidth < 5:
	plotwidth = 5
elif plotwidth > 15:
	plotwidth = 15

if len(dims) < 10:
	xtics=range(0,len(dims))
else:
	xtics=range(0,len(dims), int(math.ceil(len(dims)/15.0)))

fig_size = plt.rcParams['figure.figsize']
fig_size[0] = plotwidth
plt.rcParams['figure.figsize'] = fig_size
plt.rcParams['svg.fonttype'] = 'none'  # To have text as text and not as path

fig, ax1 = plt.subplots(1)
fig.suptitle('Mean Pearson correlation (95% CI) \nof matched IC-pairs')
ax2 = ax1.twinx()
ax1.set_zorder(ax2.get_zorder()+1)  # Put first graph on top of the second
ax1.patch.set_visible(False)  # Remove background from first axis
ax1.set_xlabel('ICA dimensionality')
ax1.xaxis.set_ticks([dims[i] for i in xtics])

ax1.plot(dims, final_means, color='black')
ax1.fill_between(dims, final_means-z_ci*data_se, final_means+z_ci*data_se, color='black', alpha=0.2)
ax1.set_ylabel('Reproducibility')
ax1.set_ylim(0,1)

ax2.bar(dims, final_errors, color='red', alpha=.6)
ax2.set_ylabel('Non-convergence', color='red')
ax2.set_ylim(0,0.2)
ax2.yaxis.set_ticks(np.arange(0, 0.51, 0.1))
ax2.set_yticklabels([0, 0.1, 0.2, 0.3, 0.4, 0.5])
ax2.tick_params(axis='y', labelcolor='red')

plt.savefig(main_folder + '/corr_mean.png')
plt.savefig(main_folder + '/corr_mean.svg', transparent=True)
plt.close()
