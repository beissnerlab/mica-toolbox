#
# mICA TOOLBOX
#

# Copyright (C) 2015 Tawfik Moher Alsady, Jorge Manuel and Florian Beissner
#
# beissner.florian@mh-hannover.de
#
# Developed at
# Somatosensory and Autonomic Therapy Research,
# Institute for Neuroradiology, Hannover Medical School,
# Hannover, Germany
#
# This file is part of the mICA toolbox.
#
# The mICA toolbox is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The mICA toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with the mICA toolbox.  If not, see <http://www.gnu.org/licenses/>.


######################
# File content:
# Part 1: main GUI setup
# Part 2: various functions to update GUI elements
# Part 3: Apply (function to be called when GO button is pressed)
# Part 4: extra functions copied and edited from fsl feat
######################



#{{{ load requirements 
source ${FSLDIR}/tcl/fslstart.tcl

source ${MICADIR}/tcl/xml_parser.tcl

set VARS(history) {}
#}}}


######################
# Begin part 1

#{{{ mICA
# Here is the main setup of the GUI elements
proc mica { w } {

    global fmri FSLDIR MICADIR PWD feat_files

    # ---- main window ----
    toplevel $w
    set fmri(mICA_version) "1.21"
    wm title $w "mICA Toolbox v$fmri(mICA_version)"
    wm iconname $w "mICA Toolbox"
    wm iconbitmap $w @${FSLDIR}/tcl/fmrib.xbm


    # ---- main frame ----
    frame $w.mica
    
    # ---- get Atlases data ----
    set fmri(atlas_list) { }
    set atlas_counter 0
    
    # 1- loop through all available atlases
    foreach file [lsort [glob -path "${FSLDIR}/data/atlases/" *.xml]] {
	
    set fmri(atlas,xml,$atlas_counter) $file

    # 2- Slurp up the data file
    set fp [open "$file" r]
    set xml [read $fp]
    close $fp
     
    # 3- parse atlas xml and save: atlas name, atlas regions and atlas image files in the following variables respectively
    # fmri(atlas,names,$atlas_counter) , fmri(atlas,labels,$atlas_counter,$label_counter) and fmri(atlas,imagefiles,$atlas_counter,$imagefile_counter)
    ::XML::Init $xml
    set wellFormed [::XML::IsWellFormed]
    if {$wellFormed ne ""} {
       puts "The atlas xml is not well-formed: $wellFormed"
    } else {
       set label_counter 0
       set next_is_atlasname 0
       set next_is_label 0
       set next_is_imagefile 0
       set imagefile_counter 0

       set fmri(atlas,labels_list,$atlas_counter) { }
       while {1} {
	  foreach {type val attr etype} [::XML::NextToken] break
	  #puts "looking at: $type '$val' '$attr' '$etype'"
	
	if {$type == "XML" && $val == "atlas" && $etype == "START"} {
    	   set next_is_atlasname 1
	   continue
	}
	if {$next_is_atlasname == 1 && $type == "TXT"} {
	   set fmri(atlas,names,$atlas_counter) $val
	   lappend fmri(atlas_list) $atlas_counter $val
	   set next_is_atlasname 0
	   continue
	}
	
	if {$type == "XML" && $val == "label" && $etype == "START"} {
	   set next_is_label 1
	   continue
	}
	if {$next_is_label == 1 && $type == "TXT"} {
	   set fmri(atlas,labels,$atlas_counter,$label_counter) $val
	   lappend fmri(atlas,labels_list,$atlas_counter) $label_counter $val
	   set next_is_label 0
	   incr label_counter 1
	   continue
	}
	
	if {$type == "XML" && $val == "imagefile" && $etype == "START"} {
	   set next_is_imagefile 1
	   continue
	}
	if {$next_is_imagefile == 1 && $type == "TXT"} {
	   set fmri(atlas,imagefiles,$atlas_counter,$imagefile_counter) "${FSLDIR}/data/atlases/$val"
	   set next_is_imagefile 0
	   incr imagefile_counter 1
	   set fmri(atlas,imagefiles_count,$atlas_counter) $imagefile_counter
	   continue
	}
	  
	if {$type == "EOF"} break
       }
    }
     
    incr atlas_counter 1
    }
    
 
    
    # ---- Set up approach selection (perform:) ----

    frame $w.mica.approach
    label $w.mica.approach.label -text "Perform: "
    set fmri(icaapproach) -single
    optionMenu2 $w.mica.approach.menu fmri(icaapproach) -command "mica:updateapproach $w" -single "Single-session ICA" -group "Multi-session (group) ICA" -backp "Back-Reconstruction" -splithalf "Reproducibility analysis"
    pack $w.mica.approach.label $w.mica.approach.menu -in $w.mica.approach -side top -side left

    balloonhelp_for $w.mica.approach "Choose one of the following tasks to perform:

    1- Single-session mICA (batch mode): Runs a single ICA for each subject.
    2- Multi-session (group) mICA: Runs a group ICA.
    3- Back-Reconstruction: Performs direct back-reconstruction (inversion) and/or dual-regression.
    4- Random Split-Half group ICA reproducibility: Runs split-half reproducibility estimation."
	    
    
    # default parameters
    set fmri(anal_min) 1
    set fmri(inputtype) 2
    set fmri(level) 1
    set fmri(analysis) 0
    set fmri(inmelodic) 1
    set fmri(dataf) $w.mica
    set fmri(multiple) 1
    set fmri(input_in_mni) 1
    set fmri(input_isotrope) 0
    
    # ---- Set up input elements ----
    # create a frame for input elements
    TitleFrame $w.mica.input -text "Input"  -relief groove
    set lfinput [ $w.mica.input getframe ]
    
    # Number and type of inputs
    frame $w.mica.multiple
    LabelSpinBox $w.mica.multiple.number -label "Number of inputs " -textvariable fmri(multiple) -range {1 1000 1 }  -width 3 -command "$w.mica.multiple.number.spin.e validate" -modifycmd ""
    optionMenu2 $w.mica.multiple.type fmri(inputtype) -command "mica:updatefeat4d $w" 1 "FEAT folder(s)" 2 "4D nifti file(s)"
    button $w.mica.multiple.setup -text "Select" -command "mica:multiple_select $w 0 \"Select input data\" "
    pack $w.mica.multiple.number $w.mica.multiple.type $w.mica.multiple.setup -in $w.mica.multiple -side left -padx 5

    balloonhelp_for $w.mica.multiple "Choose whether to select preprocessed 4D functional data (should be in nifti format), or to select FEAT output folders. When you want to perform a group ICA analysis, all files should be coregistered to the same standard space. Selecting FEAT folders will allow an automatic application of FEAT registration to standard space."

    # options for FEAT input (apply registration & func file path)
    frame $w.mica.featopt
    set fmri(applyfeatreg) 1
    set fmri(featfunc) "filtered_func_data.nii.gz"
    checkbutton $w.mica.applyfeatregch -variable fmri(applyfeatreg) -text "Apply reg2standard"
    label $w.mica.featfunclabel -text " | 4D filepath:"
    entry $w.mica.featfunctxt -textvariable fmri(featfunc) -width 25
    pack $w.mica.applyfeatregch $w.mica.featfunclabel $w.mica.featfunctxt -in $w.mica.featopt -side left -padx 5

    balloonhelp_for $w.mica.applyfeatregch "Check to apply example_func2standard transform to 4D input data."

    # resampling
    set fmri(resample) 0
    LabelSpinBox $w.mica.upsspin -label "Resampling resolution (mm) (0 for no resampling) " -textvariable fmri(resample) -range {0.0 4.0 0.5 } -modifycmd ""
	
    # smoothing
    set fmri(smooth) 0
    LabelSpinBox $w.mica.smoothspin -label "Spatial smoothing FWHM (mm) (0 for no smoothing) " -textvariable fmri(smooth) -range {0.00 1000 0.1 } -modifycmd ""
    balloonhelp_for $w.mica.smoothspin "Smooth 4D data. For masked ICA, it is suggested to restrict any smoothing to within the mask, which is the approach used here."
	
    # pack all elements in input frame
    pack $w.mica.multiple $w.mica.upsspin $w.mica.smoothspin -in $lfinput -side top -anchor w -pady 3 -padx 5

    
    # ---- mask ----
    # create a frame for mask elements
    TitleFrame $w.mica.mask -text "Mask" -relief groove
    set lfmask [ $w.mica.mask getframe ]

    # number of masks and select button
    frame $w.mica.multiplemasks
    set fmri(multiplemasks) 1
    LabelSpinBox $w.mica.multiplemasks.number -label "Number of masks: " -textvariable fmri(multiplemasks) -range {0 20 1 } -width 3 -command "$w.mica.multiplemask.number.spin.e validate;" -modifycmd ""
    button $w.mica.multiplemasks.setupbtn -text "Select" -command "mica:updateselectmask $w"
    label $w.mica.multiplemasks.selecttxt -text "Mask #:"
    pack $w.mica.multiplemasks.number $w.mica.multiplemasks.setupbtn -in $w.mica.multiplemasks -side left -padx 5
    
    balloonhelp_for $w.mica.multiplemasks "Choose one or multiple anatomical regions from the drop down menu. You can also use your own mask by selecting Other. Selected masks will be merged for the analysis. Start by entering the desired total number of (sub-)masks and press Select. Then choose those masks from the menu, one for each Mask #."

    # pack number of masks and select button in the mask frame
    pack $w.mica.multiplemasks -in $lfmask -side top -anchor w -pady 3 -padx 5
    
    # create atlas selection elements
    mica:updateselectmask $w

    
    # ---- options ----
    # create a frame for options
    TitleFrame $w.mica.opt -text "Options" -relief groove
    set lfopt [ $w.mica.opt getframe ]
	
    # create a spinbox for split-half repetitions
    set fmri(splithalfrep) 50
    LabelSpinBox $w.mica.splithalfspin -label "Repetitions of Split-Half sampling " -textvariable fmri(splithalfrep) -range {0 10000 1 } -modifycmd ""

    balloonhelp_for $w.mica.splithalfspin "Number of random samplings for split-half reproducibility estimation.
	
    Entering 0 will omit random sampling and instead compare the first half of subjects against the second half (for test-retest reproducibility analyses)."
	
    # create elements to input number of components
    frame $w.mica.dim
    set fmri(dim) 0
    label $w.mica.dim.label -text "Number of components (0 for automatic estimation) " -anchor w -justify left 
    entry $w.mica.dim.txt -textvariable fmri(dim) -width 10
    pack $w.mica.dim.label $w.mica.dim.txt -in $w.mica.dim -side top -side left
	
    balloonhelp_for $w.mica.dim "You can enter a single value, comma separated values, or a range. If more than one value is entered, the analysis is repeated for each parameter value.
	
    For example: 0 will perform a mICA using MELODIC automated dimensionality estimation.
    20 will perform a mICA with a decomposition dimensionality of 20.
    20-29 will perform 10 mICAs, one for each dimensionality between 20 and 29.
    20,30,40 will perform 3 mICAs, one for each dimensionality of 20, 30 and 40.
    0,20-29,40 is a possible input, too."

	
	# ---- Advanced options ----
        # create a collapsible frame for advanced options
	collapsible frame $w.mica.advopt -title "Advanced options" -command ""

	# select background image
	frame $w.mica.bgimage
	label $w.mica.bgimage.label -text "Background image "

	set fmri(bgimagefile) ""
	FileEntry $w.mica.bgimage.file -textvariable fmri(bgimagefile) -label  "" -title "Select" -width 30 -filedialog directory -filetypes IMAGE
	pack $w.mica.bgimage.label $w.mica.bgimage.file -in $w.mica.bgimage -side top -side left
	
	balloonhelp_for $w.mica.bgimage  "Background image for MELODIC report. If left blank, the mean functional image will be used."

	# generate parcellation checkbox
	set fmri(oparcel) 1
	checkbutton $w.mica.parcelch -variable fmri(oparcel) -text "Generate Parcellation"

	balloonhelp_for $w.mica.parcelch "Parcellate the masked region using the ICA results. Each voxel will be assigned the number of the IC with the highest z-score."

	# input field for extra melodic flags
	frame $w.mica.extra
	set fmri(cmdextra) ""
	label $w.mica.extra.label -text "Additional MELODIC command flags:" -anchor w -justify left 
	entry $w.mica.extra.txt -textvariable fmri(cmdextra) -width 20
	pack $w.mica.extra.label $w.mica.extra.txt -in $w.mica.extra -side top -side left
	
	balloonhelp_for $w.mica.extra "These arguments will be passed directly to MELODIC tool. For example: \"--approach=tica\" to use Tensor-ICA."

	# crop checkbox
 	set fmri(crop) 0
	checkbutton $w.mica.cropch -variable fmri(crop) -text "Crop input"
	
	balloonhelp_for $w.mica.cropch "Crop input images to include the smallest non-zero region of the mask. This option is highly recommended when performing reproducibility estimation as it increases the speed of the repetitive ICA analysis."
	
	
	# ---- Model & contrasts ----
	# create a collapsible frame for model and contrast files
	collapsible frame $w.mica.posts -title "Timeseries/Session model" -command ""
    
	# default variables
	set fmri(ts_model_mat) ""
	set fmri(ts_model_con) ""
	set fmri(subject_model_mat) ""
	set fmri(subject_model_con) ""
	set fmri(subject_model_grp) ""
	set fmri(subject_model_fts) ""
	
	# create input fields
	FileEntry $w.mica.posts.ts_mat      -textvariable fmri(ts_model_mat)      -label " Timeseries model                 " -title "Timeseries design.mat model" -width 30 -filedialog directory -filetypes *.mat
	FileEntry $w.mica.posts.ts_con      -textvariable fmri(ts_model_con)      -label " Timeseries contrasts           " -title "Timeseries design.con contrasts" -width 30 -filedialog directory -filetypes *.con
	FileEntry $w.mica.posts.subject_mat -textvariable fmri(subject_model_mat) -label " Session/subjects model       " -title "Session/subjects design.mat model" -width 30 -filedialog directory -filetypes *.mat
	FileEntry $w.mica.posts.subject_con -textvariable fmri(subject_model_con) -label " Session/subjects contrasts " -title "Session/subjects design.con contrasts" -width 30 -filedialog directory -filetypes *.con
	FileEntry $w.mica.posts.subject_grp -textvariable fmri(subject_model_grp) -label " Session/subjects exchangeability " -title "Session/subjects design.grp exchangeability" -width 30 -filedialog directory -filetypes *.grp
	FileEntry $w.mica.posts.subject_fts -textvariable fmri(subject_model_fts) -label " Session/subjects F-contrast " -title "Session/subjects design.fts F-contrast" -width 30 -filedialog directory -filetypes *.fts



	# ---- options for Back Projection ----
	# create a frame for options of back projection
	frame $w.mica.backp
    
	# Inversion and D-R checkboxes
	frame $w.mica.backprochf
	set fmri(doinv) 0
	checkbutton $w.mica.doinvch -variable fmri(doinv) -text "Inversion (direct Back-Reconstruction)"
	set fmri(dodr) 0
	checkbutton $w.mica.dodrch -variable fmri(dodr) -text "Dual-Regression"
	set fmri(dodrp) 0
	checkbutton $w.mica.dodrpch -variable fmri(dodrp) -text "Dual-Regression (PALM)"
	pack $w.mica.dodrch $w.mica.dodrpch $w.mica.doinvch -in $w.mica.backprochf -side top -side left
    
	set help_backp "Estimate connectivity of mICA components with other brain areas. The target region may be the whole brain or a sub-region (as defined by the target mask).
    
	\"Dual Regression\" will derive subject-specific time-courses and spatial maps and run second level analyses on these maps. It can be used to calculate group differences when supplied with second-level design matrix and contrast.
	
	\"Dual Regression (PALM)\" will derive subject-specific time-courses and spatial maps and run second level analyses on these maps. It can be used to calculate group differences when supplied with second-level design matrix and contrast. It will use PALM instead of randomise. THIS FEATURE IS EXPERIMENTAL! Take a look at dual_regression_palm to know which options are being run.
    
	\"Inversion\" will run a direct backreconstruction by using the unmixing matrix as design matrix in a GLM on the concatenated input data. It cannot be used to alculate group differences."
    
	balloonhelp_for $w.mica.doinvch $help_backp
	balloonhelp_for $w.mica.dodrch $help_backp
	balloonhelp_for $w.mica.dodrpch $help_backp
	
	# ICA directory
	set fmri(melodicdir) ""
	FileEntry  $w.mica.melodicdir -textvariable fmri(melodicdir) -label "ICA directory:" -title "Select" -width 30 -filedialog directory -filetypes { }
	
	balloonhelp_for $w.mica.melodicdir "Select the directory of components spatial maps (melodic_IC) and time courses (melodic_mix)."
	
	# ICA input
	set fmri(icainput) ""
	FileEntry  $w.mica.icainput -textvariable fmri(icainput) -label "ICA input (txt list):" -title "Select" -width 30 -filedialog directory -filetypes *
	
	balloonhelp_for $w.mica.icainput "Select a text list including input used for ICA. In case you used mICA Toolbox to perform ICA, you can select the file \"input_preproc.txt\" that is saved in ICA output directory you previously selected."
	
	# MM threshold
	set fmri(mmthresh) 0.5
	LabelSpinBox $w.mica.mmthresh -label "Mixture model threshold" -textvariable fmri(mmthresh) -range {0.0 1.0 0.10 } -modifycmd ""
	balloonhelp_for $w.mica.mmthresh "Threshold for mixture model (used only with inversion)."
	
	# Number of permutations
	set fmri(perm) 5000
	LabelSpinBox $w.mica.perm -label "Number of permutations" -textvariable fmri(perm)
	balloonhelp_for $w.mica.perm "Number of permutations during randomise (used only in dual-regression)"
	
	# pack back projection options in their containing frame
	pack $w.mica.backprochf $w.mica.melodicdir $w.mica.icainput $w.mica.mmthresh $w.mica.perm -in $w.mica.backp -anchor w -pady 5 

    
    # ---- output ----
    # create a frame for output elements
    TitleFrame $w.mica.output -text "Output" -relief groove
    set lfoutput [ $w.mica.output getframe ]

    # output dir
    set fmri(out_dir) ""
    FileEntry  $w.mica.outputdir -textvariable fmri(out_dir) -label  "Output folder" -title "Select" -width 35 -filedialog directory  -filetypes { }

    # pack elements in output frame
    pack $w.mica.outputdir -in $lfoutput -side top -anchor w -pady 3 -padx 5

    
    
    # ---- Button Frame ----
    # create a frame for go & exit buttons
    frame $w.btns
    frame $w.btns.b -relief raised -borderwidth 1

    # GO
    button $w.apply     -command "mica:apply $w" \
	    -text "Go" -width 5
    
    # Save / Load
    button $w.savebtn -command "feat_file:setup_dialog $w a a a [namespace current] *.cfg {Save mICA config} {mica:write $w} {}" -text "Save"

	button $w.loadbtn -command "feat_file:setup_dialog $w a a a [namespace current] *.cfg {Load mICA config} {mica:load $w} {}" -text "Load"

	balloonhelp_for $w.savebtn "Save selected config to file."
	balloonhelp_for $w.loadbtn "Loaf config from file."

	
    # Exit
    button $w.cancel    -command "destroy $w" \
	    -text "Exit" -width 5

    # Help
    button $w.help -command "FmribWebHelp http: www.nitrc.org/projects/mica" \
	    -text "Help" -width 5

    # pack buttons in frame
    pack $w.btns.b -side bottom -fill x -pady 5
    pack $w.apply $w.savebtn $w.loadbtn $w.cancel $w.help -in $w.btns.b \
	    -side left -expand yes -padx 3 -pady 5 -fill y

    
    # ---- pack all frames in main mica frame ----
    pack $w.mica.approach $w.mica.input $w.mica.mask $w.mica.opt $w.mica.output $w.btns -in $w.mica -side top -anchor w -pady 0 -padx 5

    # update selected approach to default
    mica:updateapproach $w
   
    # pack mica frame in the app window
    pack $w.mica -expand yes -fill both

}

#}}}


# End part 1
######################



######################
# Begin part 2


#{{{ mica:updateselectmask
# update GUI elements when 'Select' button in mask generation panel is pressed
proc mica:updateselectmask { w } {

    global mica fmri
    
    # remove previously created "mask #" drop down list
    pack forget $w.mica.multiplemasks.select
    destroy $w.mica.multiplemasks.select
    
    
    if { $fmri(multiplemasks) > 0 } {
	set fmri(selectmask) 1
    
	# set default values if not previously selected
	set selectmasklist { }
	for { set i 1 } { $i <= $fmri(multiplemasks) } { incr i 1 } {
	    lappend selectmasklist $i $i
	    if { [info exists fmri($i,atlasid)] == 0 } {
		set fmri($i,atlasid) 3
		set fmri($i,maskid) 0
		set fmri($i,maskthresh) 80
		set fmri($i,maskfile) ""
	    }
	}   
	
	# create new "mask #" drop down list
	eval optionMenu2 $w.mica.multiplemasks.select fmri(selectmask) [concat {-command "mica:updateatlasgui $w"} $selectmasklist]
	pack $w.mica.multiplemasks.selecttxt $w.mica.multiplemasks.select -in $w.mica.multiplemasks -side left -padx 5

    } else {
	# if number of masks == 0 then remove "mask #" drop down list
	pack forget $w.mica.multiplemasks.selecttxt $w.mica.multiplemasks.select

    }

    mica:updateatlasgui $w
    
}
#}}}


#{{{ mica:updateatlasgui
# update GUI elements when 'Select' button in mask generation panel is pressed
proc mica:updateatlasgui { w } {
    
    global mica fmri
    
    # get the mask frame
    set lfmask [ $w.mica.mask getframe ]

    # remove previously shown atlas selection menu and thresholding spin
    pack forget $w.mica.atlasmenu $w.mica.maskthreshspin $w.mica.maskinput
    destroy $w.mica.atlasmenu $w.mica.maskthreshspin $w.mica.maskinput
    
    # if at least 1 is given in number of masks -> create atlas selection box for the selected mask number
    if { $fmri(multiplemasks) > 0 } {
	# in MNI? -> include atlas regions and custom in the selection box
	# else -> include only custom
	if { $fmri(input_in_mni) == 0 } {
	    set fmri($fmri(selectmask),atlasid) -o
	    optionMenu2 $w.mica.atlasmenu fmri($fmri(selectmask),atlasid) -o "Custom mask"
	} else {
	    eval optionMenu2 $w.mica.atlasmenu fmri($fmri(selectmask),atlasid) [concat {-command "set fmri($fmri(selectmask),maskid) 0; mica:updatemaskgui $w"} $fmri(atlas_list) {-o "Custom mask"}]
	}
	
	# create a thresholding spin for the selected mask number
	LabelSpinBox $w.mica.maskthreshspin -label "Atlas probability threshold" -textvariable fmri($fmri(selectmask),maskthresh) -range {1 100 5 } -modifycmd ""
	balloonhelp_for $w.mica.maskthreshspin "Threshold for probabilistic atlases (e.g. tissue probability). The threshold is applied before the mask is binarized for analysis."
	
	# create a custom mask input field for the selected mask number
	FileEntry $w.mica.maskinput -textvariable fmri($fmri(selectmask),maskfile) -label "Mask nifti file" -title "Select" -width 35 -filedialog directory  -filetypes IMAGE
    
	# show atlas selection box in mask frame
	pack $w.mica.atlasmenu -in $lfmask -side top -anchor w -pady 3 -padx 5
    }
    
    # update following elements in the mask frame accordingly
    mica:updatemaskgui $w

}
#}}}


#{{{ mica:updatemaskgui
# update GUI elements when an atlas is selected
proc mica:updatemaskgui { w } {

    global mica fmri
    
    # get mask frame
    set lfmask [ $w.mica.mask getframe ]

    # remove previously shown selection box for atlas regions 
    pack forget $w.mica.maskmenu
    destroy $w.mica.maskmenu
    
    if { $fmri(multiplemasks) > 0 } {
	
	# if custom is selected -> hide thresholding spinbox and show custom input field
	if { $fmri($fmri(selectmask),atlasid) == "-o" } { 
	    pack forget $w.mica.maskthreshspin
	    pack $w.mica.maskinput -in $lfmask -side top -anchor w -pady 3 -padx 5
	    
	# if an atlas is selected -> create a new selection box that includes the atlas's regions and show it with thresholding spinbox
	} else {
	    eval optionMenu2 $w.mica.maskmenu fmri($fmri(selectmask),maskid) $fmri(atlas,labels_list,$fmri($fmri(selectmask),atlasid))
	    pack forget $w.mica.maskinput
	    pack $w.mica.maskmenu $w.mica.maskthreshspin -in $lfmask -side top -anchor w -pady 3 -padx 5
	}
    }
    
}
#}}}


#{{{ mica:updatefeat4d
# update GUI elements when FEAT/4D input selected
proc mica:updatefeat4d { w } {
    global mica fmri
    
    if { $fmri(inputtype) == 1 } {
	# show FEAT input options
	pack $w.mica.featopt -after $w.mica.multiple -side top -anchor w -pady 3 -padx 5

    } else {
	# hide FEAT input options
	pack forget $w.mica.featopt
    }
    
}
#}}}


#{{{ mica:updateapproach
# update GUI elements when an approach is selected from the main options menu
proc mica:updateapproach { w } {
    global mica fmri

	# set default parameters for:
    # reproducibility
    if { $fmri(icaapproach) == "-splithalf" } {
	
	set fmri(splithalfrep) 50
	set fmri(crop) 1
	set fmri(bgimagefile) ""
	
	#  back-projection options
    } elseif { $fmri(icaapproach) == "-backp" } {
	
	set fmri(crop) 0
	set fmri(bgimagefile) ""
	
	} else {
	
	set fmri(crop) 0
	
	}
	
	mica:updateapproachgui $w
	
}


proc mica:updateapproachgui { w } {
    global mica fmri
    
    # get options frame
    set lfopt [ $w.mica.opt getframe ]

    #single group
    # Show single-subject options and hide other irrelevant options
    if { $fmri(icaapproach) == "-single" } {
	pack forget $w.mica.posts.subject_mat $w.mica.posts.subject_con $w.mica.posts.subject_grp $w.mica.posts.subject_fts

	pack $w.mica.parcelch $w.mica.cropch $w.mica.bgimage $w.mica.extra -in $w.mica.advopt.b -anchor w -pady 5
	pack $w.mica.posts.ts_mat $w.mica.posts.ts_con -in $w.mica.posts.b -anchor w -pady 5

	pack forget $w.mica.splithalfspin $w.mica.backp
	pack $w.mica.dim $w.mica.advopt $w.mica.posts -in $lfopt -side top -anchor w -pady 3 -padx 5

	balloonhelp_for $w.mica.posts "Same functionality as in MELODIC.

	If a time-series \"design.mat\" model and \"design.con\" contrast
	file is selected (like the ones used in first-level FEAT analyses),
	these will be used in ordering the ICs, and in providing additional
	information."

    # Show group options and hide other irrelevant options
    } elseif { $fmri(icaapproach) == "-group" } {
	pack forget $w.mica.posts.subject_grp $w.mica.posts.subject_fts

	pack $w.mica.parcelch $w.mica.cropch $w.mica.bgimage $w.mica.extra -in $w.mica.advopt.b -anchor w -pady 5
	pack $w.mica.posts.ts_mat $w.mica.posts.ts_con $w.mica.posts.subject_mat $w.mica.posts.subject_con -in $w.mica.posts.b -anchor w -pady 5 

	pack forget $w.mica.splithalfspin $w.mica.backp
	pack $w.mica.dim $w.mica.advopt $w.mica.posts -in $lfopt -side top -anchor w -pady 3 -padx 5
	
	balloonhelp_for $w.mica.posts "Same functionality as in MELODIC. 
	
	If a time-series \"design.mat\" model and \"design.con\" contrast
	file is selected (like the ones used in first-level FEAT analyses),
	these will be used in ordering the ICs, and in providing additional
	information.
	
	For multi-session/multi-subjects ICAs, one can also select a subject
	\"design.mat\" model and \"design.con\" contrast file (like the ones used 
	in a higher-level FEAT analyses). These will be used in ordering the ICA
	components, and in providing additional information about the sessions/subjects."

    # Show reproducibility options and hide other irrelevant options
    } elseif { $fmri(icaapproach) == "-splithalf" } {

	pack forget $w.mica.parcelch $w.mica.bgimage

	pack $w.mica.splithalfspin $w.mica.dim $w.mica.advopt -in $lfopt -side top -anchor w -pady 3 -padx 5
	pack forget $w.mica.posts $w.mica.backp

    # Show back projection options and hide other irrelevant options
    } elseif { $fmri(icaapproach) == "-backp" } {
	
	pack forget $w.mica.posts.ts_mat $w.mica.posts.ts_con

	pack $w.mica.posts.subject_mat $w.mica.posts.subject_con $w.mica.posts.subject_fts $w.mica.posts.subject_grp -in $w.mica.posts.b -anchor w -pady 5
	pack $w.mica.backp $w.mica.posts -in $lfopt -side top -anchor w -pady 3 -padx 5
	pack forget $w.mica.dim $w.mica.advopt $w.mica.splithalfspin
	
	balloonhelp_for $w.mica.posts "Only for Dual-Regression:
 The time-series \"desing.mat\" model and \"design.con\" contrast will be used for the t-tests.
 For F-test a \"design.fts\" contrast is needed.
 The exchangeability \"design.grp\" specifies which data can be exchanged during randomisation (exchangeability blocks)."

    # show mICA options and hide other unrelevant options
    } else {
	
	pack $w.mica.parcelch $w.mica.cropch $w.mica.bgimage $w.mica.extra -in $w.mica.advopt.b -anchor w -pady 5
	
	pack $w.mica.posts.ts_mat $w.mica.posts.ts_con $w.mica.posts.subject_mat $w.mica.posts.subject_con -in $w.mica.posts.b -anchor w -pady 5 

	pack forget $w.mica.splithalfspin $w.mica.backp
	pack $w.mica.dim $w.mica.advopt $w.mica.posts -in $lfopt -side top -anchor w -pady 3 -padx 5
    }
    
}
#}}}

# End part 2
######################



######################
# Begin part 3


#{{{ mica:apply
# Apply the selected approach (called on clicking GO button)
proc mica:apply { w } {
    global fmri feat_files FSLDIR MICADIR

    puts "mica:apply"
    
    # check that more than 2 input files are supplied for group/reproducibility analysis
    if { $fmri(multiple) < 2 } {
	set nmultiple 1
	
	if { $fmri(icaapproach) == "-group" || $fmri(icaapproach) == "-splithalf" } {
	    MxPause "Error: please select more than one input to perform the selected task."
	    return 1
	}
	
    } else {
	set nmultiple $fmri(multiple)
    }
    
    # checks for back projection
    if { $fmri(icaapproach) == "-backp" } {
	# check that at least inversion or dr has been selected
	if { $fmri(doinv) == 0 && $fmri(dodr) == 0 && $fmri(dodrp) == 0 } {
	    MxPause "Error: please select inversion and/or dual-regression."
	    return 1
	}
	
	# check that ICs directory is supplied
	if { $fmri(melodicdir) == "" } {
	    MxPause "Please select ICs components directory to perform back reconstruction."
	    return 1
	}
	
	# check if following files exist: melodic_IC, melodic_mix and mask 
	if { (! [ file exists "$fmri(melodicdir)/melodic_IC.nii.gz" ]) || (! [ file exists "$fmri(melodicdir)/melodic_mix" ]) || (! [ file exists "$fmri(melodicdir)/mask.nii.gz" ]) } {
	    MxPause "Cannot find melodic_IC.nii.gz, mask.nii.gz and melodic_mix in the selected ICs components directory."
	    return 1
	}
	
	# check if ICA input list is supplied
	if { $fmri(icainput) == "" || (! [ file exists "$fmri(icainput)" ]) } {
	    MxPause "Please select a text list including the preprocessed input used in ICA."
	    return 1
	}
    }

    
    # multiple_check: checks if all input images exist and are compatible with each other
    if { [ mica:multiple_check $w 0 1 1 d ] } {
	return 1
    }
    
    # check that output directory has been selected
    # if not empty give a warning
    if { $fmri(out_dir) == "" } {
	MxPause "Please select an output directory."
	return 1
    
    } else {
	
	set file_list [ glob -nocomplain "$fmri(out_dir)/*" ]
	if { [ llength $file_list ] > 0 } {

    	    set confirmBox [ MessageDlg $w.emptyout -type "okcancel" -title "Output folder is not empty" -message "The selected output folder is not empty. Existing files might get overwritten or affect the analysis. Are you sure you want to continue?" ]
	    # if cancel pressed -> quit this function
	    if { $confirmBox == 1 } { return 1 }
	    
	}

    }
    
    # create variables to save mica commmands
    # we have two main tools to be called: mica_preproc and mica
    set preproc_options ""
    set extra_cmd ""
    
    # input is FEAT dir?
    if { $fmri(inputtype) == 1 } {
	set filename_reg "noreg"
	# apply registration checked?
	if { $fmri(applyfeatreg) == 1 } {
	    # resample resolution selected? no -> error
    	    if { $fmri(resample) == 0 } {
		MxPause "Error: please select a resampling higher than 0 in order to correctly apply FEAT registration to standard space"
		return 1
	    }
	    
	    # set 
	    set filename_reg [ mica:get_feat_func2standard $feat_files(1) ]
	    
	}
	# add feat registration and func image path to mica_preproc command
	append preproc_options " -feat $filename_reg $fmri(featfunc)"
    }
    
    # add resampling if selected to mica_preproc command
    if { $fmri(resample) != 0 } { append preproc_options " -resample $fmri(resample)" }
        
    # add smoothing if selected to mica_preproc command
    if { $fmri(smooth) != 0 } { set smooth_sigma [ expr {double($fmri(smooth)) / 2} ]; append preproc_options " -smooth ${smooth_sigma}" }

    # add mask args to mica_preproc command
    # if at least one mask is selected -> loop through all selected masks/atlas regions
    set mask_cmd ""
    if { $fmri(multiplemasks) > 0 } {
	for { set i 1 } { $i <= $fmri(multiplemasks) } { incr i 1 } {
	    
	    # is custom mask?
	    if { $fmri($i,atlasid) == "-o" } {
		set mask_file $fmri($i,maskfile)
		
		#check that selected custom mask exists and is compatible with input images
		if { $mask_file != "" && [ file exists "$mask_file" ] } {
		    
		    set xfov [exec sh -c "$FSLDIR/bin/fslval $mask_file dim1" ]
		    set yfov [exec sh -c "$FSLDIR/bin/fslval $mask_file dim2" ]
		    set zfov [exec sh -c "$FSLDIR/bin/fslval $mask_file dim3" ]
		    
		    set xdim [exec sh -c "$FSLDIR/bin/fslval $mask_file pixdim1" ]
		    set ydim [exec sh -c "$FSLDIR/bin/fslval $mask_file pixdim2" ]
		    set zdim [exec sh -c "$FSLDIR/bin/fslval $mask_file pixdim3" ]
		    
		    #does the mask fit the resolution of original input images?
		    #yes then resample mask
		    if { $xfov != $fmri(first_dims,1) || $yfov != $fmri(first_dims,2) || $zfov != $fmri(first_dims,3) } {
			#resampling?
			#yes then do not resample mask
			if { $xdim == $ydim && $xdim == $zdim && $xdim == $fmri(resample) } {
			    set confirmBox [ MessageDlg $w.emptyout -type "okcancel" -title "Mask size error" -message "The mask has a different size as input images but the same resolution as the selected resampling. The mask will not be resampled. Resampled input images may fit the selected mask, if not, an error will occur. Do you want to continue?" ]
			    # if cancel pressed then quit this function
			    if { $confirmBox == 1 } { return 1 }
			} else {
			    MxPause "The dimensions of the selected mask do not match input"
			    return 1
			}
		    } else {
			if { $fmri(resample) != 0 } { append preproc_options " -resample_mask " }
		    }
		    
		    
		    # add the custom mask to preproc_command
		    append preproc_options " -mask $mask_file "
		    
		} else {
		    MxPause "Custom mask is either empty or mask file was not found."
		    return 1
		}
	    
	    # generate mask from atlas?
	    } else {
		
		
		
		# how many image files does the selected atlas has
		set imagefiles_count $fmri(atlas,imagefiles_count,$fmri($i,atlasid))
		
		# default: use first image mentioned in the atlas file
		# if the atlas has more than one image -> select the one that suits input images resolution (or selected resampling resolution)
		set imagefile_num 0
		if { $imagefiles_count > 1 &&
		    ( $fmri(resample) > 0 || $fmri(input_isotrope) > 0 ) } {
				    
		    # default resolution to look for
		    set lookfor "-2mm"
		    
		    # if input images has isotrope resolution -> search for an atlas image that has the same resolution
		    if { [ expr $fmri(input_isotrope) ]  == 1 } { set lookfor "-1mm" }
		    if { [ expr $fmri(input_isotrope) ]  == 2 } { set lookfor "-2mm" }
    
		    # if resampling is selected -> use it to choose the best atalas image
		    if { [ expr $fmri(resample) ]  == 1 } { set lookfor "-1mm" }
		    if { [ expr $fmri(resample) ]  == 2 } { set lookfor "-2mm" }
		    
		    # search file images for the most suitable
		    for { set ii 0 } { $ii < $imagefiles_count } { incr ii 1 } {
			if { [string first $lookfor $fmri(atlas,imagefiles,$fmri($i,atlasid),$ii) ] >= 0 } {
			    set imagefile_num $ii
			}
		    }
		}
		
		# add the atlas region options to preproc_command (mica_preproc will extract the region and create a mask)
		append preproc_options " -resample_mask -atlas $fmri(atlas,imagefiles,$fmri($i,atlasid),$imagefile_num) $fmri($i,maskid) $fmri($i,maskthresh) "
	    }
	}
    
	# the expected mask file name to be generated by mica_preproc 
	set mask_cmd "-mask $fmri(out_dir)/final_mask.nii.gz"
    
    # no mask selected?
    # if back projection is selected -> error: can't perform back projection without a mask
    } elseif { $fmri(icaapproach) == "-backp" } {
	MxPause "Please select one mask at least to perform back-projection."
	return 1
    }
        

    # check that background image (for melodic report) is compatible with input images
    if { ($fmri(icaapproach) == "-single" || $fmri(icaapproach) == "-group") && $fmri(bgimagefile) != "" } {
	
	set xfov [exec sh -c "$FSLDIR/bin/fslval $fmri(bgimagefile) dim1" ]
	set yfov [exec sh -c "$FSLDIR/bin/fslval $fmri(bgimagefile) dim2" ]
	set zfov [exec sh -c "$FSLDIR/bin/fslval $fmri(bgimagefile) dim3" ]
	
	set xdim [exec sh -c "$FSLDIR/bin/fslval $fmri(bgimagefile) pixdim1" ]
	set ydim [exec sh -c "$FSLDIR/bin/fslval $fmri(bgimagefile) pixdim2" ]
	set zdim [exec sh -c "$FSLDIR/bin/fslval $fmri(bgimagefile) pixdim3" ]
		    
	#does the mask fit the resolution of original input images?
	#yes then resample mask
	if { $xfov != $fmri(first_dims,1) || $yfov != $fmri(first_dims,2) || $zfov != $fmri(first_dims,3) } {
	    #resampling?
	    #yes then do not resample mask, No then Error
	    if { $xdim == $ydim && $xdim == $zdim && $xdim == $fmri(resample) } {
		set confirmBox [ MessageDlg $w.emptyout -type "okcancel" -title "Background image size error" -message "The background image has a different size as input images but the same resolution as the selected resampling. The background image will not be resampled. Resampled input images may fit the selected background image, if not, an error will occur. Do you want to continue?" ]
		# if cancel pressed then quit this function
		if { $confirmBox == 1 } { return 1 }
		
	    } else {
		MxPause "Dimentions of the selected background image do not match input"
		return 1
	    }
	} else {
	    if { $fmri(resample) != 0 } { append preproc_options " -resample_bg " }
	}
	
	# add background iamge to preproc command
	append preproc_options " -bgimage $fmri(bgimagefile)"
	
	# add the expected filename of the preprocessed background iamge (generated by mica_preproc) to the mica command
	append extra_cmd " --bgimage=$fmri(out_dir)/bgimage.nii.gz"
    }
    

    # add cropping to preproc command
    if { $fmri(crop) == 1 } {
	 append preproc_options " -crop"
    }
    
    # add dimensionality to mica command
    set dim_cmd ""
    if { $fmri(dim) != "" } {
	set dim_cmd "-dim $fmri(dim)"
    }

    # extra args for the mica command
    set mica_extra_args "-merge-stats"
    
    # add design model and contrasts to mica command
    if { $fmri(subject_model_mat) != "" } { append mica_extra_args " -Sdes $fmri(subject_model_mat)" }
    if { $fmri(subject_model_con) != "" } { append mica_extra_args " -Scon $fmri(subject_model_con)" }
    
    if { $fmri(ts_model_mat) != "" } { append mica_extra_args " -Tdes $fmri(ts_model_mat)" }
    if { $fmri(ts_model_con) != "" } { append mica_extra_args " -Tcon $fmri(ts_model_con)" }
    
    # add generate parcellation to mica command
    if { $fmri(oparcel) != 0 } { append mica_extra_args " -parcel" }

    # add extra melodic flags to mica command
    append extra_cmd " $fmri(cmdextra) --report --Ostats"
    if { $extra_cmd != "" } { append mica_extra_args " -extra $extra_cmd" }

    # create output dir
    file mkdir $fmri(out_dir)
    
    # save config
    mica:write $w "$fmri(out_dir)/mica_config.cfg"

    # save selected input filenames to input_orig (will be passed to mica_preproc that generates a list called input_preproc)
    set all_input "$fmri(out_dir)/input_orig.txt"
    set fo [open "$all_input" "w"]
    for { set i 1 } { $i <= $nmultiple } { incr i 1 } {
	puts $fo $feat_files($i)
    }
    close $fo
	
    ## start preprocessing
    file mkdir $fmri(out_dir)/tmp_log

    # call mica_preproc command
    set prepro_cmd "${MICADIR}/bin/mica_preproc $all_input $fmri(out_dir) $preproc_options"
    #set jobid0 [ exec sh -c "${FSLDIR}/bin/fsl_sub -N mICAPrepro -T 30 -l $fmri(out_dir)/tmp_log $prepro_cmd" ]
    fsl:exec "$prepro_cmd"
    
    # after preproc has been called, call the main function (mica, inversion, dual-regression or reproducibility)
    # -----------------------------------------------
    # are we performign reproducibility analysis?
    if { $fmri(icaapproach) == "-splithalf" } {
	
	# step 1: random sampling
	# scheduled via fsl_sub for multithread processing
	set jobid_tmp [ fsl:exec "${FSLDIR}/bin/fsl_sub -N mICASplitHalf_step1 -T 5 -l $fmri(out_dir)/tmp_log python ${MICADIR}/py/splithalf.py $fmri(out_dir)/input_preproc.txt $fmri(splithalfrep) $fmri(out_dir)" ]
	set jobid1 [lindex $jobid_tmp 0]
	
	# step 2: ask mica to create melodic commands for all samplings and save them in a text file
	set maxrange 1
	if { $fmri(splithalfrep) != 0 } {
	    set maxrange $fmri(splithalfrep)
	}
	
	for { set i 1 } { $i <= $maxrange } { incr i 1 } {
	    set sample_i [format "sample_%04d" $i]
	    #puts $sample_i
	    
	    for { set g 1 } { $g <= 2 } { incr g 1 } {   
    
		fsl:exec "${MICADIR}/bin/mica $fmri(out_dir)/$sample_i/group${g}_input.txt $fmri(out_dir)/$sample_i/group$g $dim_cmd $mask_cmd -save-commands-to $fmri(out_dir)/all_melodic_commands.txt -extra $fmri(cmdextra)"
	    }
	}
	
	# scheduled all melodic commands via fsl_sub
	puts "${FSLDIR}/bin/fsl_sub -N mICASplitHalf_step2 -T 60 -l $fmri(out_dir)/tmp_log -j ${jobid1} -t $fmri(out_dir)/all_melodic_commands.txt"
	set jobid_tmp [ fsl:exec "${FSLDIR}/bin/fsl_sub -N mICASplitHalf_step2 -T 60 -l $fmri(out_dir)/tmp_log -j ${jobid1} -t $fmri(out_dir)/all_melodic_commands.txt" ]
	puts jobid_tmp
	set jobid2 [lindex $jobid_tmp 0]

	# step 3: calculate correlation -> reproducibility
	# scheduled via fsl_sub for multithread processing
	puts "${FSLDIR}/bin/fsl_sub -N mICASplitHalf_step3 -T 30 -l $fmri(out_dir)/tmp_log -j $jobid2 python ${MICADIR}/py/ic_corr.py $fmri(out_dir) $fmri(splithalfrep) $fmri(dim)"
	fsl:exec "${FSLDIR}/bin/fsl_sub -N mICASplitHalf_step3 -T 30 -l $fmri(out_dir)/tmp_log -j $jobid2 python ${MICADIR}/py/ic_corr.py $fmri(out_dir) $fmri(splithalfrep) $fmri(dim)"

	
    # -----------------------------------------------
    # are we performing batch single subject mICA?
    } elseif { $fmri(icaapproach) == "-single" } {
	
	# loop through input iamges and call mica command for each preprocessed one
	set fp [open "$fmri(out_dir)/input_preproc.txt" r]
	set file_data [read $fp]
	close $fp
	set data [split $file_data "\n"]
	for { set i 0 } { $i < $nmultiple } { incr i 1 } {

	    set subjid [ expr $i + 1 ]    
	    set subjdir [format "subj_%04d" $subjid]
	    file mkdir "$fmri(out_dir)/$subjdir"
	    
	    set subjfile "$fmri(out_dir)/$subjdir/input_preproc.txt"
	    set foSubj [open "$subjfile" "w"]
	    puts $foSubj [ lindex $data $i ]
	    close $foSubj
    
	    file mkdir "$fmri(out_dir)/$subjdir/tmp_log"
	    fsl:exec "${FSLDIR}/bin/fsl_sub -N mICASingleSubject -l $fmri(out_dir)/$subjdir/tmp_log -T 30 ${MICADIR}/bin/mica $subjfile $fmri(out_dir)/$subjdir $dim_cmd $mask_cmd $mica_extra_args"
	    
	}
	
    # -----------------------------------------------
    # are we performing group mICA?
    } elseif { $fmri(icaapproach) == "-group" } {
	
	# call mica command for preprocessed input list (input_preproc)
	file mkdir "$fmri(out_dir)/tmp_log"
	fsl:exec "${FSLDIR}/bin/fsl_sub -N mICAGroup -l $fmri(out_dir)/tmp_log -T 30 ${MICADIR}/bin/mica $fmri(out_dir)/input_preproc.txt $fmri(out_dir) $dim_cmd $mask_cmd $mica_extra_args"
	
    
    # -----------------------------------------------
    # are we performing back-projection?
    } elseif { $fmri(icaapproach) == "-backp" } {

	set backpjobid_cmd ""
	
	# inversion
	if { $fmri(doinv)!= 0 } {
	    file mkdir "$fmri(out_dir)/inversion"

	    set inv_cmd "${MICADIR}/bin/ica_inversion $fmri(out_dir)/input_preproc.txt $fmri(melodicdir) $fmri(out_dir)/inversion ${mask_cmd} -mmthresh $fmri(mmthresh)"
	    fsl:exec "$inv_cmd"
	    #set backpjobid [ exec sh -c "${FSLDIR}/bin/fsl_sub -N mICAInversion -l $fmri(out_dir)/tmp_log -T 30 $inv_cmd" ]
	    #set backpjobid_cmd "-j $backpjobid"
	}
	
	# dual-regression
	if { $fmri(dodr) != 0 } {
	    file mkdir "$fmri(out_dir)/dR"
    
	    set SdesScon_cmd "-1"
	    if { $fmri(subject_model_mat) != "" && $fmri(subject_model_con) != "" } { set SdesScon_cmd "$fmri(subject_model_mat) $fmri(subject_model_con)" }
	    set Sfts_cmd "0"
	    if { $fmri(subject_model_fts) != "" } { set Sfts_cmd "$fmri(subject_model_fts)" }
	    set Sgrp_cmd "0"
	    if { $fmri(subject_model_grp) != "" } { set Sgrp_cmd "$fmri(subject_model_grp)" }
	
	    set dr_cmd "${MICADIR}/bin/dual_regression_roi $fmri(melodicdir)/melodic_IC.nii.gz 1 $fmri(melodicdir)/mask.nii.gz $fmri(out_dir)/final_mask.nii.gz $SdesScon_cmd ${Sfts_cmd} ${Sgrp_cmd} $fmri(perm) $fmri(out_dir)/dR -in-step1 \$(cat $fmri(icainput)) -in-step2 \$(cat $fmri(out_dir)/input_preproc.txt)"
	    fsl:exec "$dr_cmd"
	    #fsl:exec "${FSLDIR}/bin/fsl_sub -N mICAdR -l $fmri(out_dir)/tmp_log -T 30 ${backpjobid_cmd} $dr_cmd"
	}
	
	# dual-regression (PALM)
	if { $fmri(dodrp) != 0 } {
	    file mkdir "$fmri(out_dir)/dR"
    
	    set SdesScon_cmd "-1"
	    if { $fmri(subject_model_mat) != "" && $fmri(subject_model_con) != "" } { set SdesScon_cmd "$fmri(subject_model_mat) $fmri(subject_model_con)" }
	    set Sfts_cmd "0"
	    if { $fmri(subject_model_fts) != "" } { set Sfts_cmd "$fmri(subject_model_fts)" }
	    set Sgrp_cmd "0"
	    if { $fmri(subject_model_grp) != "" } { set Sgrp_cmd "$fmri(subject_model_grp)" }
	
	    set dr_cmd "${MICADIR}/bin/dual_regression_palm $fmri(melodicdir)/melodic_IC.nii.gz 1 $fmri(melodicdir)/mask.nii.gz $fmri(out_dir)/final_mask.nii.gz $SdesScon_cmd ${Sfts_cmd} ${Sgrp_cmd} $fmri(perm) $fmri(out_dir)/dR -in-step1 \$(cat $fmri(icainput)) -in-step2 \$(cat $fmri(out_dir)/input_preproc.txt)"
	    fsl:exec "$dr_cmd"
	}

    }
    
    
    update idletasks
    puts "Done.. Please check the output folder and logs"
    MxPause "All tasks have been scheduled/performed. Please check the output folder and the logs inside."
}

#}}}



# this function checks if image has an isotropic voxel size
proc mica:check_BB { img1 x y z } {
    
    set xfov1 [exec sh -c "$FSLDIR/bin/fslval $img1 pixdim1" ]
    set yfov1 [exec sh -c "$FSLDIR/bin/fslval $img1 pixdim2" ]
    set zfov1 [exec sh -c "$FSLDIR/bin/fslval $img1 pixdim3" ]

    if { xfov1 == x && yfov1 == y && zfov1 == z } {
	return 1;
    } else {
	return 0;
    }
}



# this function checks if FEAT directory includes registration files
proc mica:get_feat_func2standard { featdir } {
				
    if { [ file exists ${featdir}/reg/example_func2standard_warp.nii.gz ] } {
	return "stdwarp"
    } elseif { [ file exists ${featdir}/reg/example_func2standard.mat ] } {
	return "std"
    } else {
	return "noreg"
    }
    
}



# End part 3
######################


######################
# Begin part 4


# mica:multiple_select
# this function is for selecting multiple input images
proc mica:multiple_select { w which_files windowtitle } {
    global FSLDIR MICADIR fmri VARS PWD feat_files unwarp_files unwarp_files_mag initial_highres_files highres_files confoundev_files alt_ex_func

    # setup window

    set count 0
    set w0 ".dialog[incr count]"
    while { [ winfo exists $w0 ] } {
        set w0 ".dialog[incr count]"
    }

    toplevel $w0

    wm iconname $w0 "Select"
    wm iconbitmap $w0 @${FSLDIR}/tcl/fmrib.xbm

    wm title $w0 $windowtitle

    frame $w0.f
    pack $w0.f -expand yes -fill both -in $w0 -side top

    canvas $w0.f.viewport -yscrollcommand "$w0.f.ysbar set"
    scrollbar $w0.f.ysbar -command "$w0.f.viewport yview" -orient vertical
    frame $w0.f.viewport.f
    $w0.f.viewport create window 0 0 -anchor nw -window $w0.f.viewport.f
    bind $w0.f.viewport.f <Configure> "feat5:scrollform_resize $w0 $w0.f.viewport"
    pack $w0.f.viewport -side left -fill both -expand true -in $w0.f

    # setup file selections

    set pastevar feat_files

    for { set i 1 } { $i <= $fmri(multiple) } { incr i 1 } {
    if { $which_files < 1 } {
	if { $fmri(inputtype) == 2 } {
            FileEntry $w0.filename$i -textvariable feat_files($i) -filetypes IMAGE 
	} else {
            FileEntry $w0.filename$i -textvariable feat_files($i) -filetypes *.feat -dirasfile design.fsf
	}
    }
    
    $w0.filename$i configure  -label " $i:   " -title "Select input data" -width 60 -filedialog directory
    pack $w0.filename$i -in $w0.f.viewport.f -side top -expand yes -fill both -padx 3 -pady 3
    }

    #
    # setup buttons
    
    frame $w0.btns
    frame $w0.btns.b -relief raised -borderwidth 1
    
    button $w0.pastebutton -command "feat5:multiple_paste \"Input data\" 1 $fmri(multiple) $pastevar x" -text "Paste"
    
    button $w0.cancel -command "mica:multiple_check $w $which_files 1 1 d; destroy $w0" -text "OK"
    
    pack $w0.btns.b -side bottom -fill x -padx 3 -pady 5
    pack $w0.pastebutton $w0.cancel -in $w0.btns.b -side left -expand yes -padx 3 -pady 3 -fill y
    pack $w0.btns -expand yes -fill x
    
    #
}


#
# mica:multiple_check
# modified from fsl
# this function checks input images and makes sure they are compatible with each other
proc mica:multiple_check { w which_files load updateimageinfo { dummy dummy } } {
    global FSLDIR MICADIR fmri feat_files unwarp_files unwarp_files_mag initial_highres_files highres_files confoundev_files alt_ex_func
    if { $which_files < 0 } {
	set load 0
	featquery_whichstats $w
    }

   
    if { $fmri(multiple) < 2 } {
	set nmultiple 1
    } else {
	set nmultiple $fmri(multiple)
    }

    set AllOK ""

    #set mni2mmhd [ exec sh -c [concat "${FSLDIR}/data/standard/MNI152_T1_2mm.nii.gz" { | grep 'qto\|sto'}] ]
    #set mni1mmhd [ exec sh -c [concat "${FSLDIR}/data/standard/MNI152_T1_1mm.nii.gz" { | grep 'qto\|sto'}] ]
    #set mniqformname [ exec sh -c [concat "${FSLDIR}/data/standard/MNI152_T1_2mm.nii.gz" { | grep 'qform_name'}] ]

    set all_in_mni 1
    set first_is_isotrope 0

    for { set i 1 } { $i <= $nmultiple } { incr i 1 } {

	if { $which_files == 0 } {

	    if { ! [ info exists feat_files($i) ] } {
		set feat_files($i) ""
	    }
	    if { ! [ file exists $feat_files($i) ] && ! [ imtest $feat_files($i) ] } {
		set AllOK "${AllOK}
Problem with input file ($i): currently set to \"$feat_files($i)\""
	    } else {
		
		
		set filename_reg_extra ""
		if { $fmri(inputtype) == 1 } {
		    #puts "FEAT"
		
		    if { ! [ file exists $feat_files($i)/$fmri(featfunc) ] } {
			set AllOK "${AllOK}
Problem with input FEAT directory ($i): currently looking for \"$feat_files($i)/$fmri(featfunc)\""

		    } else {

			set func2std [ mica:get_feat_func2standard $feat_files($i) ]
			if { $func2std != "noreg" && $fmri(applyfeatreg) == 1 } {
				
			    if { $func2std == "stdwarp" } {
				set warp_nowarp_name "/reg/example_func2standard_warp.nii.gz"
				set filename_reg_extra "/reg/standard.nii.gz"
			    } elseif { $func2std == "std" } {
				set warp_nowarp_name "/reg/example_func2standard.mat"
				set filename_reg_extra "/reg/standard.nii.gz"
			    } elseif { $func2std == "highres" } {
				set warp_nowarp_name "/reg/example_func2highres.mat"
				set filename_reg_extra "/reg/highres.nii.gz"
			    }
			    
			    if { ! [ file exists $feat_files($i)$warp_nowarp_name ] } {
				set AllOK "${AllOK}
Problem with input FEAT directory ($i): currently looking for \"$feat_files($i)$warp_nowarp_name\""
			    }
			    

			} else {
			    set filename_reg_extra "/$fmri(featfunc)"
			}

		    }
		}
		    
		if { $AllOK == "" } {

		    set xfov [exec sh -c "$FSLDIR/bin/fslval $feat_files($i)$filename_reg_extra dim1" ]
	            set yfov [exec sh -c "$FSLDIR/bin/fslval $feat_files($i)$filename_reg_extra dim2" ]
		    set zfov [exec sh -c "$FSLDIR/bin/fslval $feat_files($i)$filename_reg_extra dim3" ]
		    
		    if {  $i == 1 } {
			set fmri(first_dims,1) $xfov
			set fmri(first_dims,2) $yfov
			set fmri(first_dims,3) $zfov
			
			set xfov_pix [exec sh -c "$FSLDIR/bin/fslval $feat_files($i)$filename_reg_extra pixdim1" ]
			set yfov_pix [exec sh -c "$FSLDIR/bin/fslval $feat_files($i)$filename_reg_extra pixdim2" ]
			set zfov_pix [exec sh -c "$FSLDIR/bin/fslval $feat_files($i)$filename_reg_extra pixdim3" ]
			if { $xfov_pix == $yfov_pix && $xfov_pix == $zfov_pix } {
			    set first_is_isotrope $xfov_pix
			}
			
		    } elseif { $xfov != $fmri(first_dims,1) || $yfov != $fmri(first_dims,2) || $zfov != $fmri(first_dims,3) } {
			set AllOK "${AllOK}
Error: not all images have the same size"
		    }

		    if { ($xfov != 91 || $yfov != 109 || $zfov != 91)
			&& ($xfov != 182 || $yfov != 218 || $zfov != 182)
			&& ($xfov != 364 || $yfov != 436 || $zfov != 364) } {
			
			set all_in_mni 0
		    }
		    
		    set filehd [ exec sh -c [concat "${FSLDIR}/bin/fslhd $feat_files($i)$filename_reg_extra" { | grep 'qform_name'}] ]
		    if { [string first "MNI_152" $filehd ] < 0 } {
			#puts "not in mni: $filehd"
			set all_in_mni 0
		    }

		
		}
		

		
	    }

	}
    }

    if { $AllOK != "" } {
	MxPause "Error: you haven't filled in all the relevant selections with valid filenames:

$AllOK"
	return 1
    }
    
    
    if { $all_in_mni == 0 } {
	set fmri(input_in_mni) 0
	#MxPause "Warning: the images are not in MNI space. Atlas-based mask generation will be deactivated. You have to specify a custom mask."

    } else {
	set fmri(input_in_mni) 1
	set fmri(input_isotrope) $first_is_isotrope
	
	if { $first_is_isotrope == 0 } {
	    #MxPause "Warning: the images resolution is not isotropic. If no resabling is selected, an Atlas-based mask will be resambled "
	} elseif { $first_is_isotrope != 1  && $first_is_isotrope != 2 } {
	    #MxPause "Warning: the images resolution is not isotropic. If no resabling is selected, an Atlas-based mask will be resambled "
	}
    }
    mica:updateatlasgui $w
    
   
    return 0
}

# mica:write
proc mica:write { w filename } {

    global fmri FSLDIR MICADIR PWD feat_files

    set filename [ file rootname $filename ]

    set channel [ open ${filename}.cfg "w" ]

    # save variables
    puts $channel "

## MAIN ##
# mICA Toolbox version number
# Version: $fmri(mICA_version)

# approach
set fmri(icaapproach) $fmri(icaapproach)

## INPUT ##
# input type
set fmri(inputtype) $fmri(inputtype)

# input count
set fmri(multiple) $fmri(multiple)

# input filenames
"
	for { set i 1 } { $i <= $fmri(multiple) } { incr i 1 } {
		if { [info exists feat_files($i)] == 1 } {
			puts $channel "set feat_files($i) $feat_files($i)
"
		} else {
			puts $channel "set feat_files($i) \"\"
"
		}
	}

	puts $channel "
# apply reg2standard
set fmri(applyfeatreg) $fmri(applyfeatreg)

# 4D filepath
set fmri(featfunc) $fmri(featfunc)

# resampling
set fmri(resample) $fmri(resample)
	
# smoothing
set fmri(smooth) $fmri(smooth)

## MASKS ##
# number of masks
set fmri(multiplemasks) $fmri(multiplemasks)

# masks
"

	for { set i 1 } { $i <= $fmri(multiplemasks) } { incr i 1 } {
		if { [info exists fmri($i,atlasid)] == 1 } {
		puts $channel "set fmri($i,atlasid) $fmri($i,atlasid)
set fmri($i,maskthresh) $fmri($i,maskthresh)
set fmri($i,maskid) $fmri($i,maskid)
set fmri($i,maskfile) $fmri($i,maskfile)
"
		if { $fmri($i,maskfile) ne ""} {
			puts $channel "set fmri($i,maskfile) \"$fmri($i,maskfile)\"
"
		} else {
			puts $channel "set fmri($i,maskfile) \"\"
"		
		}
		
		}
	}   

	puts $channel "

## OPTIONS ##
# split-half repetitions
set fmri(splithalfrep) $fmri(splithalfrep)

# Number of components
set fmri(dim) \"$fmri(dim)\"

"

	if { $fmri(ts_model_mat) ne ""} {
	puts $channel "# design model
set fmri(ts_model_mat) $fmri(ts_model_mat)
"
	}
	if { $fmri(ts_model_con) ne ""} {
	puts $channel "# design model
set fmri(ts_model_con) $fmri(ts_model_con)
"
	}
	if { $fmri(subject_model_mat) ne ""} {
	puts $channel "# design model
set fmri(subject_model_mat) $fmri(subject_model_mat)
"
	}
	if { $fmri(subject_model_con) ne ""} {
	puts $channel "# design model
set fmri(subject_model_con) $fmri(subject_model_con)
"
	}

if { $fmri(subject_model_fts) ne "" && $fmri(icaapproach) eq "-backp"} {
	puts $channel "# design model
set fmri(subject_model_fts) $fmri(subject_model_fts)
"
	}

if { $fmri(subject_model_grp) ne "" && $fmri(icaapproach) eq "-backp"} {
	puts $channel "# design model
set fmri(subject_model_grp) $fmri(subject_model_grp)
"
	}

	if { $fmri(bgimagefile) ne ""} {
	puts $channel "# background
set fmri(bgimagefile) $fmri(bgimagefile)
"
	}
	
	if { $fmri(cmdextra) ne ""} {
	puts $channel "# extra melodic flags
set fmri(cmdextra) \"$fmri(cmdextra)\"
"
	}
	
	if { $fmri(melodicdir) ne ""} {
	puts $channel "# Back-reconstruction: melodic directory
set fmri(melodicdir) $fmri(melodicdir)
"
	}
	if { $fmri(icainput) ne ""} {
	puts $channel "# Back-reconstruction: ICA input
set fmri(icainput) $fmri(icainput)
"
	}

	if { $fmri(icaapproach) eq "-backp" } {
	puts $channel "# Perform Inversion 
set fmri(doinv) $fmri(doinv)

# Perform Dual-Regression
set fmri(dodr) $fmri(dodr)

# Perdorm dual regression with PALM
set fmri(dodrp) $fmri(dodrp)

# MM threshold
set fmri(mmthresh) $fmri(mmthresh)

# Number of permutations
set fmri(perm) $fmri(perm)
"
	}

	puts $channel "
# Perform parcellation
set fmri(oparcel) $fmri(oparcel) 

# Perform cropping
set fmri(crop) $fmri(crop)


## OUTPUT ##
"
	if { $fmri(out_dir) ne ""} {
	puts $channel "# output dir
set fmri(out_dir) $fmri(out_dir)
"
	}

    close $channel
    
}


proc mica:load { w filename } {
    global fmri FSLDIR MICADIR PWD feat_files

    source $filename
    
    mica:updateapproachgui $w
    mica:multiple_check $w 0 1 1 d
	mica:updateselectmask $w

}

# End part 4
######################


#{{{ call GUI

wm withdraw .
mica .rename
tkwait window .rename

#}}}
